+++
title = 'Mac File Location'
date = 2024-01-06T18:40:18-08:00
draft = false
+++
|  name |  location | 
|---|---|
| Safari Bookmarks	|`~/Library/Safari/Bookmarks.plist` | 
| Safari Bookmarks	|`~/Library/Safari/Bookmarks.plist` | 
| Safari History 	|`~/Library/Safari/History.db` | 
| Chrome Bookmarks	|`~/Library/Application\ Support/Google/Chrome/Default/Bookmarks` | 
| Chrome History 	|`~/Library/Application\ Support/Google/Chrome/Default/History` | 
| Firefox Bookmarks |``	| 
| Google Profile	|`~/Library/Application\ Support/Google/Chrome` | 
| Photo Location 	|`~/Pictures/Photos\ Library.photoslibrary/originals` |

+++
title = 'Mac Setup'
date = 2023-12-05T22:05:39-08:00
weight = 5
draft = false
+++

1. Install one Note and get the instruction 
    1. https://rectangleapp.com 
2. Change terminal settings (minimum)  
    1. Default to HomeBrew  
    2. Text tab -> Change font to Melano 18  
    3. Shell Tab -> change to close if exited cleanly  
3. 3 finger drag: https://support.apple.com/en-us/HT204609
4. Install Code 
    1. https://code.visualstudio.com/docs/setup/mac
5. Install Warp 
6. Install 1Password /BitWarden
7. Install Data Dog Agent to monitor Disk usage? 
    1. https://us5.datadoghq.com/signup/agent
8. Install oh-my-zsh 
    1. https://ohmyz.sh/#install  
    `sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"`  
    `ZSH_THEME="robbyrussell" to ZSH_THEME="pygmalion"`
10. Install Disk Space Analyzer, change trackpad setting, change name 
11. Install magnet, change trackpad setting, change name 
12. Install onedrive and one note -- need to write instruction to do JDK and run it properly 
13. Install something to check HDD size and track as I install new s/w
14. Install Java (JdiskReport) 
15. https://sourabhbajaj.com/mac-setup/
    1. Install commandline tool for xcode 
    2. Install brew 
16. `brew install zsh`
17. `brew install git`
18. `brew install git-gui`
19. `brew install bash-completion`
20. `brew cask install visual-studio-code`
21. `brew install tree`
22. `brew install speedtest-cli`
23. Install python (pyshim)
24. Start here - Python
25. Install office 
26. `brew install --cask finicky`

+++
title = 'Launchd'
date = 2023-12-26T18:33:02-08:00
draft = false
+++

First thing to do is create a script to run. I’m going to write a simple Node.js but you can use whatever you want.

`mkcd ~/demo/`  
`touch check.sh`  
Open the file and insert this to the file: 
```
now=$(date +"%T")
echo "Current time : $now"
```
Change the file permission so that it can execulte 
`chmod +x check.sh`  

Another option is to create a python file:   
`touch main.py`  
`code main.py`  

```
import datetime

print("Hello, the time now is: ")
print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
```

Create a file for the launchd
`touch ~/Library/LaunchAgents/com.demo.daemon.plist`

```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
  <dict>
    <key>Label</key>
    <string>com.demo.daemon.plist</string>

    <key>RunAtLoad</key>
    <true/>

    <key>StartInterval</key>
    <integer>20</integer>

    <key>StandardErrorPath</key>
    <string>$HOME/demo/stderr.log</string>

    <key>StandardOutPath</key>
    <string>$HOME/demo/stdout.log</string>

    <key>EnvironmentVariables</key>
    <dict>
      <key>PATH</key>
      <string><![CDATA[/usr/local/bin:/usr/local/sbin:/usr/bin:/bin:/usr/sbin:/sbin]]></string>
    </dict>

    <key>WorkingDirectory</key>
    <string>$HOME/demo</string>

    <key>ProgramArguments</key>
    <array>
      <string>/Users/ai/.pyenv/shims/python</string>
      <string>$HOME/demo/main.py</string>
    </array>

  </dict>
</plist>
```
`launchctl load ~/Library/LaunchAgents/com.demo.daemon.plist`  
`launchctl unload ~/Library/LaunchAgents/com.demo.daemon.plist`  
`launchctl start ~/Library/LaunchAgents/com.demo.daemon.plist`  

These are some helpful pages   
https://www.launchd.info    
https://github.com/eddiezane/lunchy  
https://alvinalexander.com/mac-os-x/launchd-examples-launchd-plist-file-examples-mac/    
https://medium.com/@chetcorcos/a-simple-launchd-tutorial-9fecfcf2dbb3  
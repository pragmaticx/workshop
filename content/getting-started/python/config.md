+++
title = 'Config'
date = 2024-01-21T20:24:41-08:00
draft = false
+++

Let me start a simple confing to help me based on: 
- https://docs.python.org/3/library/configparser.html
- https://pymotw.com/3/configparser/  

Here is a simple config file, names `config.ini` 

```
[DEFAULT]
user_root = /Users/ai/
hotel_root = /Users/ai/Google Drive/My Drive/Hotel
repo_root = /Users/ai/repos/
gdrive = /Users/ai/Google Drive/My Drive
data_root = /Users/ai/Google Drive/My Drive/Data
db_root = 

[deepspace.local]
user_root = /Users/ai/
hotel_root = /Users/ai/Google Drive/My Drive/Hotel
repo_root = /Users/ai/repos/
gdrive = /Users/ai/Google Drive/My Drive

```

Now, lets code `config.py` to read thse values from the file `config.ini`

```
import configparser
import socket 

config = configparser.ConfigParser()
hostname = socket.gethostname()

config.read('config.ini')
print(config.sections())


deepspace = config[hostname]
hotel_root = deepspace['hotel_root']
print(hotel_root)

print(config['DEFAULT']['data_root'])
```
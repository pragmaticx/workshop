+++
title = 'Gitignore'
date = 2024-01-07T19:05:17-08:00
draft = false
+++

```
# See https://help.github.com/articles/ignoring-files/ for more about ignoring files.

/client/node_modules
/server/node_modules
__pycache__

# dependencies
/node_modules
/.pnp
.pnp.js
.env 

# testing
/coverage

# production
/build
/bin

# misc
data/out
*.log
*.DS_Store
.env.local
.env.development.local
.env.test.local
.env.production.local

npm-debug.log*
yarn-debug.log*
yarn-error.log*

```
+++
title = 'Getting Started'
date = 2023-12-04T21:54:28-08:00
weight = 5
draft = false
+++

##### 1. Installing Git - Mac 
`brew install git `

##### 2. Configuring Git
+ add git ignore 
+ configure name 
+ configure email 

- to see all the git config  
`git config --list`

- to get a specific config  
`git config --get remote.origin.url`  

- to change git config 
`git config user.name "John Doe"`  




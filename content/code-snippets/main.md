+++
title = 'Main'
date = 2023-12-28T11:38:14-08:00
draft = false
+++

```
#!/usr/bin/env python3
"""
Module Docstring
"""
__author__ = "First Last"
__version__ = "0.1.0"
__license__ = "MIT"

def main():
    """ Main entry point of the app """
    print("hello world")

if __name__ == "__main__":
    """ This is executed when run from the command line """
    main()
```
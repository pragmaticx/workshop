https://www.markdownguide.org/basic-syntax/

Heading: 

# Heading level 1
## Heading level 2
### Heading level 3
#### Heading level 4
##### Heading level 5
###### Heading level 6

Formatting: 
**bold**
I just love **bold text**.

*Italic*
Italicized text is the *cat's meow*.
Italicized text is the _cat's meow_.

URLs and Email Addresses: 
[Example](http://www.example.com)


Image: 
![The San Juan Mountains are beautiful!](/assets/images/san-juan-mountains.jpg "San Juan Mountains")


